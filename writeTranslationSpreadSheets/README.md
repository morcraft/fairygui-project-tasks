## Write Translation SpreadSheets task

This task is meant to be run after "translateDictionaries" has been executed. It will read process.cwd()/translation/translationTargets.json, so it can be run from any folder containing that structure. It will read the values of translationTargets.json as file names in the same folder, and output them as Excel SpreadSheets.

After this task has been executed, you should use the Excel SpreadSheets as the core of the translation process. As this will make it easier for translators, designers, QA and developers to work on the same translation sources.