import path = require('path')
const chalk = require('chalk')
import * as AssetSync from 'fairygui-assets-synchronizer'
import * as ExcelManager from 'excel-dictionary-manager'

const logger = AssetSync.makeLogger(AssetSync.loggerOptions)

export interface TranslationConfiguration{
    dictionary: AssetSync.Dictionary
    translationTargets: AssetSync.Dictionary
    translationTargetsLocation: string
}

export interface DictionaryCollection{
    [s: string]: AssetSync.Dictionary
}

export interface WorkBookCollection{
    [s: string]: ExcelManager.WorkBook
}

AssetSync.readFileAndLog(path.join(process.cwd(), 'translation', 'chinese.json'), logger)
.then(buffer => {
    return JSON.parse(buffer.toString()) as AssetSync.Dictionary
})
.then(chineseDictionary => {
    return new Promise<TranslationConfiguration>((resolve, reject) => {
        const translationTargetsLocation = path.join(process.cwd(), 'translation', 'translationTargets.json')
        AssetSync.readFileAndLog(translationTargetsLocation, logger)
        .then(buffer => {
            resolve({
                translationTargetsLocation,
                dictionary: chineseDictionary,
                translationTargets: JSON.parse(buffer.toString())
            })
        })
        .catch(error => reject(error))
    })
})
.then(translationConfiguration => {
    return new Promise<DictionaryCollection>((resolve, reject) => {
        const taskQueue = new AssetSync.TaskQueue
        const dictionaries = {}
        logger.info(`Found translation targets ${chalk.green(JSON.stringify(translationConfiguration.translationTargets))}`)
        Object.keys(translationConfiguration.translationTargets).forEach(target => {
            taskQueue.addTask(target)
            AssetSync.readFileAndLog(path.join(process.cwd(), 'translation', `${target}.json`), logger)
            .then(buffer => {
                dictionaries[target] = JSON.parse(buffer.toString())
                taskQueue.completeTask(target, _ => { resolve(dictionaries) })
            })
            .catch(error => {
                logger.error(`Error while reading translation target '${target}' present in ${translationConfiguration.translationTargetsLocation}`)
                reject(error)
            })
        })
    })
})
.then(translatedDictionaries => {
    return new Promise<WorkBookCollection>((resolve, reject) => {
        const dictionaries: WorkBookCollection = {}
        Object.keys(translatedDictionaries).forEach(dictionaryName => {
            const dictionary = translatedDictionaries[dictionaryName]
            const workBook = ExcelManager.makeExcelTranslationFile(dictionary, 'Chinese', dictionaryName)
            dictionaries[dictionaryName] = workBook
        })
        resolve(dictionaries)
    })
})
.then(workBooks => {
    return new Promise((resolve, reject) => {
        const taskQueue = new AssetSync.TaskQueue
        Object.keys(workBooks).forEach(dictionaryName => { 
            taskQueue.addTask(dictionaryName)
            const targetPath = path.join(process.cwd(), 'translation', `${dictionaryName}.xlsx`)
            workBooks[dictionaryName].xlsx.writeFile(targetPath)
            .then(_ => {
                logger.info(chalk.cyan(`${targetPath} ✔`))
                taskQueue.completeTask(dictionaryName, _ => { resolve() })
            }, error => {
                reject(error)
            })
            .catch(error => { reject(error) })
        })
    })
})
.catch(error => {
    logger.error(error)
})