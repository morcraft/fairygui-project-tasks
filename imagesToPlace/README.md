This folder should contain all the pictures the designers translate and need to be added in the game. The matching of files is done based on their names and the language suffix they appear with, e.g.

    庄.png
    闲_CN2.png
    闲_EN.png
    闲_IN.png
    闲_JP.png
    闲_KR.png
    闲_TH.png
    闲_VN.png

This format is necessary to identify which pictures correspond to each language. The pictures synchronization script will take care of finding missing translations in this folder and adding them wherever they appear throughout the game components, and also assigning each one the corresponding language controller page. Once these pictures are added in the game, you may and should remove them, as they'll be picked up again the next time you run the script, and all the attributes of the pictures will be reset to those of the original pictures in the game.