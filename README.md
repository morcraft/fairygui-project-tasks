## FairyGUI project tasks

This repository should contain common tasks associated to different game features, such as translation, responsiveness, etc.

In order to run them you need to transpile the TypeScript files into plain JavaScript files which later you can run with NodeJS. The easiest way to do this is to install TypeScript as a global dependency, as such:

    npm install typescript -g
    tsc sourceFile (the .ts extension can be dropped)
    node sourceFile (the .js extension can be dropped)

Check the documentation of each task to understand the way they're meant to be run.

The dependencies they rely on were made public on GitLab, so you may clone them and modify them as you need, or request developer access. Keep in mind these dependencies are linked to each other in their package.json files, so if you want to create new repositories from the original dependencies, you need to update these.