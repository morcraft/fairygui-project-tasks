## Extract dictionaries from SpreadSheets task

This task will simply recursively retrieve all the Excel spreadsheets found under the current working directory and export them as JSON files. 

These translations SpreadSheets don't need to be created manually, since there's already a task for that. However, you may modify them as you wish if it's needed to add or remove translation keys.

It will recursively find all the Excel SpreadSheets in the process.cwd(), so you may run this task from any folder containing said SpreadSheets. 

It should output all of them as JSON files. You may then add these definitions in the /ts/language/dictionary folder, and use them freely throughout the game files by invoking:

    TSWrapper.languageAssociator.getTextFromKey(dictionaryKey, optionalArguments?)

Read the source code of the TSWrapper library for more information.