import path = require('path')
const chalk = require('chalk')
import * as AssetsSync from 'fairygui-assets-synchronizer'
import * as ExcelManager from 'excel-dictionary-manager'

const logger = AssetsSync.makeLogger(AssetsSync.loggerOptions)

//We get all the spreadsheets
AssetsSync.getFilesByExtensions(process.cwd(), ExcelManager.excelExtensions)
.then(spreadSheets => {

    logger.info(`Found ${chalk.green(Object.keys(spreadSheets).length)} spreadsheets under ${process.cwd()}`)

    return new Promise<ExcelManager.WorkBookCollection>((resolve, reject) => {
        //A queue of tasks to control their completion
        const spreadSheetQueue = new AssetsSync.TaskQueue(spreadSheets)

        //We store the virtual spreadsheet provided by exceljs
        //in this collection, which will be later resolved
        const workBooks: ExcelManager.WorkBookCollection = {}

        spreadSheets.forEach((filePath, index) => {
            ExcelManager.readExcelWorkBook(filePath)
            .then(workBook => {
                workBooks[filePath] = workBook
                //This should resolve only when all of the tasks are done
                spreadSheetQueue.completeTask(filePath, () => { resolve(workBooks) })
            }, error => {
                reject(error)
            })
            .catch(error => {
                reject(error)
            })
        })
    })
})
.then(workBooks => {
    return new Promise<AssetsSync.DictionaryCollection>((resolve, reject) => {
        try{
            //This is a hash table containing the langauge name as the key
            //and the dictionary with the Chinese keys and their translations as the value
            const dictionaries = ExcelManager.getWorkBooksAsDictionaries(workBooks, 1, 3)
            const dictionaryKeys = Object.keys(workBooks)
            
            //we create a base dictionary for
            //Chinese language, since this doesn't
            //and shouldn't exist in the dictionaries folder
            if(dictionaryKeys.length){
                //take the first dictionary and use the first and second column
                //of the spreadsheet as key and value
                const key = dictionaryKeys[0]
                const originPath = path.dirname(key)
                const fileExtension = AssetsSync.getFileExtension(path.basename(key))
                const fileName = `Chinese.${fileExtension}`
                const workBook = ExcelManager.getWorkBookAsDictionary(workBooks[key], 1, 2)
                const targetPath = path.join(originPath, fileName)
                dictionaries[targetPath] = workBook
            }

            resolve(dictionaries)
        }
        catch(error){ 
            reject(error) 
            return
        }
    })
})
.then(dictionaries => {
    return new Promise<boolean>((resolve, reject) => {
        //store the the dictionary paths in an array
        const dictionaryKeys = Object.keys(dictionaries)

        //another queue of tasks, since the following code will be run asynchronously
        const taskQueue = new AssetsSync.TaskQueue(dictionaryKeys)

        dictionaryKeys.forEach(dictionaryPath => {
            const originPath = path.dirname(dictionaryPath)
            const fileName = AssetsSync.getFileNameWithoutExtension(path.basename(dictionaryPath))
            //we use the same origin path as the translation spreadsheet, and the same node
            const targetPath = path.join(originPath, `${fileName}.json`)
            const fileString = JSON.stringify(dictionaries[dictionaryPath], null, '\t')
            AssetsSync.writeFileAndLog(targetPath, fileString, logger)
            //this should resolve when all of the tasks are done
            .then(_ => { taskQueue.completeTask(dictionaryPath, _ => resolve(true) ) })
            .catch(error => {
                reject(error)
                return false
            })
        })
    })
})
.then(state => {
    if(state)
        logger.info(chalk.green('All done!'))
    else
        logger.red(chalk.ref(`Something went wrong :c`))
})
.catch(error => {
    logger.error(chalk.red(error))
    throw error
})