import gitRootDir = require('git-root-dir')
import path = require('path')
const chalk = require('chalk')

//this package contains common methods for FairyGUI
//projects manipulation
import * as AssetsSync from 'fairygui-assets-synchronizer'

//We'll be reading Excel translation spreadsheets, so we don't
//need to extract any translations manually, or work with JSON files
import * as ExcelManager from 'excel-dictionary-manager'

//in this case the default language suffixes are used, but these may change according to the game
const languageSuffixMap = AssetsSync.languageSuffixMap

//this is a map of the language names with their corresponding suffixes, e.g. English: EN, TraditionalChinese: CN2
const languageMap = AssetsSync.languageMap

//the controller name we'll use across all the components
const controllerName = '__language'

//a simple file logger that will output in the process.cwd()
const logger = AssetsSync.makeLogger(AssetsSync.loggerOptions)

//a list of language suffixes: e.g. ['CN', 'EN', 'JP']
const languageSuffixes = ['CN'].concat(Object.keys(languageSuffixMap))

//we'll use this Regex to match the IDs of elements to identify translation nodes
const languageSuffixRegex = AssetsSync.makeSuffixRegex(Object.keys(languageSuffixMap))

//In some cases, we don't have all the translations available at once, due to many reasons.
//Regardless of that, we can still run the script, making sure first to use hyphens in the cells
//in the translation spreadsheet where the translations are missing
//The script will update this text the next time it's run if there's a translation available, while
//keeping the already translated text in the FairyGUI project untouched
const temporaryText = '---'

export interface ProjectConfiguration{
    projectPaths: string[]
    dictionaryCollection: AssetsSync.DictionaryCollection
}

//recursively get all the Excel spreadsheets in the current folder
AssetsSync.getFilesByExtensions(process.cwd(), ExcelManager.excelExtensions)
.then(spreadSheets => {
    
    logger.info(`Found ${chalk.green(Object.keys(spreadSheets).length)} spreadsheets under ${process.cwd()}`)

    return new Promise<ExcelManager.WorkBookCollection>((resolve, reject) => {
        //we use a task queue to control task storage and completion,
        //since most of the following processes are asynchronous
        const spreadSheetQueue = new AssetsSync.TaskQueue(spreadSheets)

        //a hash table containing the dictionary name e.g. English, Chinese as key
        //and an Excel virtual workbook as value
        const workBooks: ExcelManager.WorkBookCollection = {}
        spreadSheets.forEach(filePath => {
            //const fileLocation = path.dirname(filePath)
            const dictionaryName = path.basename(filePath, path.extname(filePath))
            ExcelManager.readExcelWorkBook(filePath)
            .then(workBook => {
                workBooks[dictionaryName] = workBook
                //this promise will resolve when all of the tasks are finished
                spreadSheetQueue.completeTask(filePath, () => { resolve(workBooks) })
            }, error => {
                reject(error)
            })
            .catch(error => {
                reject(error)
            })
        })
    })
})
.then(workBooks => {
    return new Promise<ProjectConfiguration>((resolve, reject) => {
        gitRootDir(process.cwd())
        .then(rootDir => {
            //remember the Git root dir we're using here is that of the project itself,
            //so this task will fail if it's living inside a folder that's also a Git repository
            logger.info(`Detected Git repository in ${chalk.green(rootDir)}`)

            //we can use as many paths as we want, but these must coincide
            //with the respective project folders
            const MainUI = path.join(rootDir, 'ui', 'assets', 'MainUI')
            const mobileMainUI = path.join(rootDir, 'mobile_ui', 'assets', 'mobileMainUI')

            //workBooksAsDictionaries refers to simple hash tables that use the Chinese text
            //as a key, and the translated text as the value, thus the 2 and 3  
            resolve({
                projectPaths: [MainUI, mobileMainUI],
                dictionaryCollection: ExcelManager.getWorkBooksAsDictionaries(workBooks, 2, 3)
            })     
        })
        .catch(error => {
            reject(error)
        })
    })
})
.then(projectConfiguration => {
    //GraphicSync.writeFileAndLog(path.join(process.cwd(), 'checkYo.json'), JSON.stringify(projectConfiguration, null, '\t'), logger)
    return new Promise<AssetsSync.Dictionary>((resolve, reject) => {

        //this sanitization is required
        //Excel arbitrarily inserts carriage return characters (\r) in the spreadsheets, and exceljs picks them up,
        //so this generates a conflict when the text is going to be matched against the translations
        Object.keys(projectConfiguration.dictionaryCollection).forEach(dictionaryName => {
            projectConfiguration.dictionaryCollection[dictionaryName] = AssetsSync.sanitizeDictionary(projectConfiguration.dictionaryCollection[dictionaryName])
        })

        //A dictionary containing the component's path as the key,
        //and the DOM representation as a string
        const filesToWrite: AssetsSync.Dictionary = {}

        //create a queue of tasks that contains the project paths
        //again, this is done asynchronously, so we need to control the completion of all the
        //asynchronous processes
        const projectsQueue = new AssetsSync.TaskQueue(projectConfiguration.projectPaths)

        //this should be executed when all the tasks are done
        const onProjectsDone = () => resolve(filesToWrite)

        //this operation is done for all the project paths
        projectConfiguration.projectPaths.forEach(projectPath => {

            //find all the components in the project folder
            AssetsSync.getFilesByExtensions(projectPath, AssetsSync.componentExtensions)
            .then(componentPaths => {
                
                //add all the components to the task queue
                const componentsQueue = new AssetsSync.TaskQueue(componentPaths)

                const onComponentsDone = () => projectsQueue.completeTask(projectPath, onProjectsDone)
                
                componentPaths.forEach(componentPath => {
                    AssetsSync.readFileAndLog(componentPath, logger)
                    .then(componentBuffer => {
                        const componentDOM = AssetsSync.getDOM(AssetsSync.DOMParser, componentBuffer)
                        //componentChildren here refers to the component nodes living inside the "component.xml" file
                        //Usually it's just one, but this should return them all anyway
                        const componentChildren = AssetsSync.getComponentChildren(componentDOM)
                        
                        //logger.info(`Component path is ${componentPath}`)
                        if(!Array.isArray(componentChildren) || !componentChildren.length){
                            //if this file doesn't contain any components, we don't need to process it or rewrite it
                            componentsQueue.completeTask(componentPath, onComponentsDone)
                            return true
                        }
                        
                        //we're going to operate on the first component node only
                        const component = componentChildren[0]

                        //this appens a language controller in the component with the language suffixes as page names
                        AssetsSync.addLanguageController(componentDOM, component, languageSuffixes, controllerName)
                        const texts = component.getElementsByTagName('text')
                        const textsLength = texts.length

                        //we assume the component doesn't need to be rewritten,
                        //that is, it doesn't contain any text nodes that have translations available
                        //in the translation spreadsheets
                        let rewriteComponent = false
                        
                        for(let i = 0; i < textsLength; i++){
                            const textAttr = texts[i].getAttribute('text')
                            const id = texts[i].getAttribute('id')

                            //all the text nodes should contain a name and an ID, but we just need the ID here
                            if(typeof id != 'string' || !id.length){
                                reject(new Error(`Invalid id for text element in component ${componentPath} containing text ${textAttr}`))
                                return
                            }

                            //log this if you're not sure what it contains
                            let translations: AssetsSync.Dictionary

                            try{
                                translations = AssetsSync.getTranslationForText(textAttr, projectConfiguration.dictionaryCollection, languageMap)
                            }
                            catch(error){ 
                                reject(error) 
                                return 
                            }
                            
                            const translationKeys = Object.keys(translations).sort()

                            //we enter this block only if the text contains translations.
                            //Up until this point we assume this component doesn't need to be rewritten
                            if(translationKeys.length){
                                const matches = id.match(languageSuffixRegex)

                                //if id contains a language suffix, we skip it, since it might already have its own
                                //modified attributes
                                if(!matches){
                                    //we remove all the gears associated to the $controllerName in this text node
                                    //and create a new display gear
                                    AssetsSync.removeLanguageGears(texts[i], controllerName)
                                    AssetsSync.addDisplayGear(componentDOM, texts[i], controllerName)

                                    //this code is run for all the translations available for this text
                                    translationKeys.forEach((suffix, suffixIndex) => {
                                        const languageSuffix = languageSuffixMap[suffix]
                                        if(!languageSuffix){
                                            reject(new Error(`Couldn't find language suffix for ${suffix}`))
                                            return
                                        }
        
                                        //IDs of elements with the suffix of the language are used to identify previous translation rounds
                                        //and keep the text elements linked to one another. So we check if this text node has already been 
                                        //translated for the current language in the loop
                                        const translatedNodes = AssetsSync.getElementsByTagAndAttribute(componentDOM, 'text', 'id', `${id}_${suffix}`)

                                        //this is a special case where some translations weren't available by the time
                                        //the script was run (this script can be run as many times as needed), so hyphens "---"
                                        //were used as a value in the cell of the translation spreadsheet
                                        translatedNodes.forEach(element => {
                                            const translatedTextAttr = element.getAttribute('text')
                                            //this checks if the text this node contains is temporary
                                            if(translatedTextAttr.replace('\\n', '').indexOf(temporaryText) > -1){
                                                //logger.info(`Element with id ${id}_${suffix} in ${componentPath} contains temporary text! New text from ${suffix} translation: "${translations[suffix]}"`)
                                                //we replace it with the new text in the translation spreadsheet
                                                element.setAttribute('text', translations[suffix])
                                                //logger.info(`New text: ${element.getAttribute('text')}`)
                                            }
                                        })

                                        //if this node hasn't been translated and added in the project yet
                                        if(!translatedNodes.length){
                                            //logger.info(`No translated nodes were found for id ${id} in ${componentPath}`)
                                            //this method takes care of adding the translated text, and modifying the id and name attributes
                                            //so that they can be identified in future translation rounds.
                                            //It also assigns the right page in the controller, so it will be displayed or shown according
                                            //to the selected page in the language controller
                                            const clonedNode = AssetsSync.appendTranslatedTextNode(texts[i], suffix, suffixIndex, controllerName)
                                            clonedNode.setAttribute('text', translations[suffix])
                                        }
                                    })
        
                                    if(!rewriteComponent){
                                        rewriteComponent = true
                                        //logger.info(`${componentPath} will be rewritten :3`)
                                    }
                                }
                            }

                        }

                        if(rewriteComponent){
                            filesToWrite[componentPath] = AssetsSync.XMLSerializer.serializeToString(componentDOM)
                        }

                        //logger.info(`Just finished task ${componentPath}`)
                        componentsQueue.completeTask(componentPath, onComponentsDone)
                    })
                    .catch(error => {
                        reject(error)
                        return
                    })
                })
            })
        })
    })
})
.then(filesToWrite => {
    return new Promise<boolean>((resolve, reject) => {
        const keys = Object.keys(filesToWrite)
        const taskQueue = new AssetsSync.TaskQueue(keys)
        //GraphicSync.writeFileAndLog(path.join(process.cwd(), 'filesToWrite.json'), JSON.stringify(filesToWrite, null, '\t'), logger)
        //we write all the translated components asynchronously and resolve when they're all done
        keys.forEach(filePath => {
            AssetsSync.writeFileAndLog(filePath, filesToWrite[filePath], logger)
            .then(_ => {
                taskQueue.completeTask(filePath, _ => { resolve(true) })
            })
            .catch(error => {
                reject(error)
                return
            })
        })
    })
})
.then(state => {
    if(state)
        logger.info(chalk.green('All done!'))
    else
        logger.red(chalk.ref(`Something went wrong :c`))
})
.catch(error => {
    logger.error(chalk.red(error))
    throw error
})