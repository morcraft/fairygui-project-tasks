## Synchronize Text task

Once these Excel SpreadSheets are read and transformed into dictionaries, all the components found in XML files in the specified paths (check synchronizeText.ts) will be read asynchronously and analyzed to determine whether they contain text nodes that have available translations in said SpreadSheets. 

It will automatically add a language controller with each target language as a page, so the designer or developer can freely assign any controller Gears to any element within a game component.

All the text nodes that contain translations will be processed and assigned a language controller page, and all the game components will be rewritten.