const chalk = require('chalk')
import fs = require('fs-extra')
import path = require('path')
import * as AssetSync from 'fairygui-assets-synchronizer'

const logger = AssetSync.makeLogger(AssetSync.loggerOptions)
const scriptExtensions = ['js', 'ts']
const chineseCharacterRegex = /['|"](.*?)[\u4E00-\u9FA5]+(.*?)["|']/gu

new Promise<string[]>((resolve, reject) => {
    const targetPath = process.cwd()
    const fileList = []
    fs.readdir(targetPath)
    .then(fileNameList => {
        fileNameList.forEach(fileName => {
            const ext = AssetSync.getFileExtension(fileName).toLowerCase()
            if(scriptExtensions.includes(ext)){
                fileList.push(path.join(targetPath, fileName))
            }
        })
        resolve(fileList)
    }, error => { reject(error) })
    .catch(error => { reject(error) })
})
.then(filePaths => {
    return new Promise<AssetSync.Dictionary>((resolve, reject) => {
        const taskQueue = new AssetSync.TaskQueue
        const chineseDictionary: AssetSync.Dictionary = {}
        if(!filePaths.length) resolve(chineseDictionary)
        filePaths.forEach(filePath => {
            taskQueue.addTask(filePath)
            AssetSync.readFileAndLog(filePath, logger)
            .then(buffer => {
                const fileContent = buffer.toString()
                const matches = fileContent.match(chineseCharacterRegex)
                if(matches instanceof Array){
                    matches.forEach(match => {
                        const escapedString = match.replace(/["']/g, "")
                        chineseDictionary[escapedString] = escapedString
                    })
                }
                    
                taskQueue.completeTask(filePath, _ => { resolve(chineseDictionary) })
            })
            .catch(error => {
                reject(error)
            })
        })
    })
})
.then(lineList => {
    const targetPath = path.join(process.cwd(), 'linesWithChinese.json')
    return AssetSync.writeFileAndLog(targetPath, JSON.stringify(lineList, null, '\t'), logger)
})
.catch(error => {
    logger.error(chalk.red(error))
})