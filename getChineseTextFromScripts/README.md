## Get Chinese Text from Scripts task

This task will crawl through the script (.js) files found under the process.cwd(), so you may run this task from the folder containing the game logic scripts, or any scripts you want to extract the lines containing Chinese text from.

There are plenty of tools out there that do the same task, so as long as all the lines with Chinese text get picked up, you're safe using any text crawling tool that supports Regex expressions.