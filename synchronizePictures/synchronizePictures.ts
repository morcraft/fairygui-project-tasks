import gitRootDir = require('git-root-dir')
import path = require('path')
const chalk = require('chalk')

//this package contains common methods for FairyGUI
//projects manipulation
import * as AssetsSync from 'fairygui-assets-synchronizer'

//in this case the default language suffixes are used, but these may change according to the game
const languageSuffixMap = AssetsSync.languageSuffixMap

//the controller name we'll use across all the components
const controllerName = '__language'

//we'll use this Regex to match the IDs of elements to identify translation nodes
const languageSuffixRegex = AssetsSync.makeSuffixRegex(Object.keys(languageSuffixMap))

//a list of language suffixes: e.g. ['CN', 'EN', 'JP']
const languageSuffixes = ['CN'].concat(Object.keys(languageSuffixMap))

//a simple file logger that will output in the process.cwd()
const logger = AssetsSync.makeLogger(AssetsSync.loggerOptions)

//a map containing the base images and their translations
let translationMap: AssetsSync.TranslationMap

const imageSourcePath = path.join(process.cwd(), 'imagesToPlace')

//get all the pictures Recursively inside $imageSourcePath and store their paths in an array
AssetsSync.getFilesByExtensions(imageSourcePath, AssetsSync.imageExtensions)
.then(pictures => {
    return new Promise<string>((resolve, reject) => {
        
        //the matching of files is done based on their name in the $imageSourcePath
        //and the FairyGUI project folder
        const baseNames = pictures.map(picture => path.basename(picture))
    
        //hash table with base names as keys
        const imagesToPlace = AssetsSync.arrayToMap(baseNames)
        translationMap = AssetsSync.getImagesTranslationMap(baseNames, languageSuffixRegex)
    
        //use for reference
        AssetsSync.writeFileAndLog(path.join(process.cwd(), 'translationMap.json'), JSON.stringify(translationMap, null, '\t'), logger)
        AssetsSync.writeFileAndLog(path.join(process.cwd(), 'imagesToPlace.json'), JSON.stringify(imagesToPlace, null, '\t'), logger)
    
        //we store any inconsistencies in the files and their names,
        //such as missing language suffixes
        const translationDifferences = AssetsSync.getTranslationDifferences(imagesToPlace, languageSuffixMap, translationMap, languageSuffixRegex)
        
        if(Object.keys(translationDifferences).length){
            const filesToCheckPath = path.join(process.cwd(), 'filesToCheck.json')
            AssetsSync.writeFileAndLog(filesToCheckPath, JSON.stringify(translationDifferences, null, '\t'), logger)
            throw new Error(`Found translation inconsistencies. Check ${filesToCheckPath}`)
        }

        //if no translation inconsistencies are found, then resolve
        //the promise with the root directory of the Project
        gitRootDir(process.cwd())
        .then(rootDir => resolve(rootDir))
        .catch(error => reject(error))
    })
})
.then(rootDir => {
    logger.info(`Detected Git repository in ${chalk.cyanBright(rootDir)}`)
    return new Promise<string[]>((resolve, reject) => {
        
        //we can send as many project paths as needed
        //keep in mind these folder names must coincide with the path
        //of the FairyGUI projects
        const MainUI = path.join(rootDir, 'ui', 'assets', 'MainUI')
        const mobileMainUI = path.join(rootDir, 'mobile_ui', 'assets', 'mobileMainUI') 
        resolve([MainUI, mobileMainUI])
    })
})
.then(projectPaths => {

    //we use a simple queue of tasks to control the state
    //and completion of the asynchronous operations
    //this proves times faster than running the following code synchronously
    const assetsQueue = new AssetsSync.TaskQueue
    
    projectPaths.forEach(projectPath => {
        //the package.xml file contains definitions for URL-referenced
        //elements in the project
        const packagePath = path.join(projectPath, 'package.xml')

        let packageDOM: Document

        //we use a queue of completion for each project.
        //This queue and the above could be merged into one.
        //However, it might be easier to control which project gets done first
        const componentsQueue = new AssetsSync.TaskQueue

        AssetsSync.readFileAndLog(packagePath, logger)
        .then(buffer => {
            packageDOM = AssetsSync.getDOM(AssetsSync.DOMParser, buffer)
            return AssetsSync.getFilesByExtensions(projectPath, AssetsSync.componentExtensions)
        })
        .then(componentPaths => {
            //$componentPaths is an array of paths as strings,
            //it should contain all the component paths in the project folder
            componentPaths.forEach(componentPath => {
                //check this method for implementation details
                AssetsSync.processComponent({
                    projectPath,
                    componentPath,
                    packagePath,
                    packageDOM,
                    componentsQueue,
                    assetsQueue,
                    languageSuffixMap,
                    languageSuffixes,
                    languageSuffixRegex,
                    translationMap,
                    imageSourcePath,
                    controllerName,
                    logger,
                })
            })
        })
    })
})
.catch(error => {
    logger.error(error)
    console.error(chalk.red(error)) //node6.2 swallows up errors in promise chains, it seems...
})
