## Synchronize Pictures task

This task is one of the most important ones in the translation process, as it will be responsible for adding the pictures the designers translate in the game project, wherever they appear. This includes veryfying the integrity of the translated assets, copying assets, reading component XML files, creating XML nodes for each picture, creating language controllers in all the components where these pictures appear, and assigning the corresponding language controller page to each asset.

Modify carefully if it's needed. The source code is documented as well.