import path = require('path')
const chalk = require('chalk')
import apiKey from './yandexTranslatorKey'
import * as GraphicSync from 'fairygui-assets-synchronizer'
import * as YandexTranslator from 'yandex-translator'

const logger = GraphicSync.makeLogger(GraphicSync.loggerOptions)

export interface TranslationConfiguration{
    dictionary: GraphicSync.Dictionary
    translationTargets: GraphicSync.Dictionary
}

export interface DictionaryCollection{
    [s: string]: GraphicSync.Dictionary
}

GraphicSync.readFileAndLog(path.join(process.cwd(), 'translation', 'chinese.json'), logger)
.then(buffer => {
    return JSON.parse(buffer.toString()) as GraphicSync.Dictionary
})
.then(chineseDictionary => {
    return new Promise<TranslationConfiguration>((resolve, reject) => {
        GraphicSync.readFileAndLog(path.join(process.cwd(), 'translation', 'translationTargets.json'), logger)
        .then(buffer => {
            resolve({
                dictionary: chineseDictionary,
                translationTargets: JSON.parse(buffer.toString())
            })
        })
        .catch(error => reject(error))
    })
})
.then(translationConfiguration => {
    return new Promise<DictionaryCollection>((resolve, reject) => {
        logger.info(`Dictionary contains ${Object.keys(translationConfiguration.dictionary).length} keys to translate`)
        const targets = translationConfiguration.translationTargets
        const taskQueue = new GraphicSync.TaskQueue
        const translatedDictionaries: DictionaryCollection = {}
        const translationTargets = Object.keys(targets)

        function translate(targetIndex: number){
            const translationTarget = translationTargets[targetIndex]
            taskQueue.addTask(translationTarget)
            logger.info(`Translating dictionary into ${chalk.cyan(translationTarget)}...`)
            
            YandexTranslator.translateDictionary(YandexTranslator.API, apiKey, translationConfiguration.dictionary, targets[translationTarget])
            .then(translatedDictionary => {
                translatedDictionaries[translationTarget] = translatedDictionary
                taskQueue.completeTask(translationTarget, _ => { 
                    if(targetIndex == translationTargets.length - 1){
                        resolve(translatedDictionaries)
                    }
                    else{
                        translate(targetIndex + 1)
                    }
                })
            })
            .catch(error => { reject(error) })
        }

        if(translationTargets.length){
            translate(0)
        }
        else{
            resolve(translatedDictionaries)
        }
    })
})
.then(translatedDictionaries => {
    return new Promise((resolve, reject) => {
        const taskQueue = new GraphicSync.TaskQueue
        const keys = Object.keys(translatedDictionaries)
        if(!keys.length){
            logger.info(`No dictionaries found to translate`)
            resolve()
        }

        keys.forEach(dictionaryName => {
            taskQueue.addTask(dictionaryName)
            const targetPath = path.join(process.cwd(), 'translation', `${dictionaryName}.json`)
            const fileContent = JSON.stringify(translatedDictionaries[dictionaryName], null, '\t')
            GraphicSync.writeFileAndLog(targetPath, fileContent, logger)
            .then(_ => {
                taskQueue.completeTask(dictionaryName, _ => { resolve() })
            })
            .catch(error => { reject(error) })
        })
    })
})
.catch(error => {
    logger.error(error)
})