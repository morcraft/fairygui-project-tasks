## Translate Dictionaries task

This task is an essential part in the translation process, it may be run for testing purposes as well. It will use an online translation API (Yandex) to create dummy translations and generate base translation SpreadSheets.

If you just need to create empty translation SpreadSheets, you can delete the cells containing the translations from the Yandex API after they're generated when you run this script.

It works by reading process.cwd()/translation/chinese.json, so you may run this task from any folder containing such a structure. This file is automatically generated by the "scanChineseText" task, so you don't need to and shouldn't create it manually.

What you need to create is a JSON file under /translation named "translationTargets.json" containing the language names as keys and the language ID string as the value. This repository contains a sample file. These IDs are the ones used by Yandex, so you need to check the corresponding ID in case you want to translated to other languages.

The output will be translated JSON files with each language as a file name. These will be later picked up by the task "writeTranslationSpreadSheets", which turns them into Excel SpreadSheets.

Keep in mind the translation process is meant to be based on such Excel SpreadSheets and not JSON files.