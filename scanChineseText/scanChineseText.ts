import path = require('path')
import gitRootDir = require('git-root-dir')
const chalk = require('chalk')
import * as AssetSync from 'fairygui-assets-synchronizer'

const logger = AssetSync.makeLogger(AssetSync.loggerOptions)

new Promise<string>((resolve, reject) => {
    gitRootDir(process.cwd())
    .then(rootDir => resolve(rootDir))
    .catch(error => reject(error))
})
.then(rootDir => {
    logger.info(`Found Git root directory in ${chalk.cyan(rootDir)}`)
    return new Promise<string[]>((resolve, reject) => {
        try{
            const MainUI = path.join(rootDir, 'ui', 'assets', 'MainUI')
            const mobileMainUI = path.join(rootDir, 'mobile_ui', 'assets', 'mobileMainUI') 
            resolve([MainUI, mobileMainUI])
        }
        catch(error){ reject(error) }
    })
})
.then(projectPaths => {
    return new Promise<AssetSync.Dictionary>((resolve, reject) => {
        logger.info(`Scanning for text in components under folders: \n${chalk.cyan(projectPaths.join('\n'))}`)
        const dictionary: AssetSync.Dictionary = {}
        const componentsQueue = new AssetSync.TaskQueue

        projectPaths.forEach(projectPath => {

            AssetSync.getFilesByExtensions(projectPath, AssetSync.componentExtensions)
            .then(componentPaths => {
                componentPaths.forEach(componentPath => {
                    componentsQueue.addTask(componentPath)

                    AssetSync.readFileAndLog(componentPath, logger)
                    .then(componentBuffer => {

                        const componentDOM = AssetSync.getDOM(AssetSync.DOMParser, componentBuffer)
                        const componentChildren = AssetSync.getComponentChildren(componentDOM)
        
                        if(!Array.isArray(componentChildren) || !componentChildren.length){
                            componentsQueue.completeTask(componentPath, _ => { resolve(dictionary) })
                            return true
                        }
        
                        const component = componentChildren[0]
                        const texts = component.getElementsByTagName('text')
                        const textsLength = texts.length
                        for(let i = 0; i < textsLength; i++){
                            const textAttr = texts[i].getAttribute('text')
                            if(AssetSync.isChineseString(textAttr)){
                                dictionary[textAttr] = textAttr
                            }
                        }

                        componentsQueue.completeTask(componentPath, _ => { resolve(dictionary) })
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            })
            .catch(error => {
                reject(error)
            })
        })
    })
})
.then(dictionary => {
    const keys = Object.keys(dictionary)
    const entries = keys.length
    const entriesText = `${entries} ${entries > 1 ? `entries` : `entry`}`
    logger.info(`Successfully built dictionary with ${chalk.green(entriesText)}. Writing file...`)
    const targetPath = path.join(process.cwd(), 'translation', 'chinese.json')
    return AssetSync.writeFileAndLog(targetPath, JSON.stringify(dictionary, null, '\t'), logger)
})
.then(() => {
    logger.info(chalk.green('All done!'))
})
.catch(error => {//node6.2
    logger.error(error)
    console.error(error)
})