## Scan Chinese Text task

This task will find all the Chinese text across the game components inside the given paths, and output a single JSON dictionary "chinese.json" under process.cwd() containing the Chinese strings as keys and values. The output file can then be picked up by the script found under "translateDictionaries", which is explained in its own README.md

