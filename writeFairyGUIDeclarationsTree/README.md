## Write FairyGUI Declarations Tree task

This task is used for development purposes and has been added automatically in the WebPack build. It generates TypeScript declaration files for each entry point that is added in the script. 

The purpose of these declaration files is to enable Type-checked member hierarchy trees, that you can use anywhere you need to check member access errors during development time.

Normally it is only during runtime that failed member accesses show up as errors. The purpose of this task is to enable member auto-completion, access errors, tree previewing during development, and the additional advantages of TypeScript.

It will output one declaration file for each entry poing that it's given under process.cwd(), so you may run this script from anywhere within the game project.

Demo:

![Demo](https://media.giphy.com/media/RJ1ypy4kRD4FR2Y2F0/giphy.gif)

/ts/myFile.ts

The script is pretty straight-forward. It finds the closest Git repository and assumes it as the base path of the game repository. 
The rest of the task is given to the [fairygui-typescript-tree-generator dependency](https://gitlab.com/morcraft/fairygui-typescript-tree-generator)

