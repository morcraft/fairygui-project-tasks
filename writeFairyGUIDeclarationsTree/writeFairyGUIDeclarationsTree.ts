import TypescriptGenerator from 'fairygui-typescript-tree-generator'
import gitRootDir = require('git-root-dir')
import path = require('path')
const chalk = require('chalk')

gitRootDir(process.cwd())
.then(rootDir => {
    console.log(`Detected Git repository in ${chalk.cyanBright(rootDir)}`)
    const MainUI = path.join(rootDir, 'ui', 'assets', 'MainUI')
    const mobileMainUI = path.join(rootDir, 'mobile_ui', 'assets', 'mobileMainUI')
    new TypescriptGenerator(MainUI, 'Main.xml').process(process.cwd(), 'mainUI')
    new TypescriptGenerator(mobileMainUI, 'Main.xml').process(process.cwd(), 'mobileMainUI')
})
.catch(error => {
    console.error(chalk.red(error)) //node6.2 swallows up the error in the promise chain, it seems...
})